classdef Utils
    %UTILS Summary of this class goes here
    %   Detailed explanation goes here

    properties
        %Propiedades de la clase
    end

    methods
        function plotRGB(self, img)
            figure,
            subplot(221),imshow(img),title('RGB');
            subplot(222),imshow(img(:,:,1)),title('R');
            subplot(223),imshow(img(:,:,2)),title('G');
            subplot(224),imshow(img(:,:,3)),title('B');
        end

        function plotHSV(self, img)
            img_hsv = rgb2hsv(img);
            figure,imshow(img),title('RGB');
            figure,imshow(img_hsv(:,:,1), []),title('H');
            figure,imshow(img_hsv(:,:,2), []),title('S');
            figure,imshow(img_hsv(:,:,3), []),title('V');
        end

        function img_hist(self, img)
            figure,
            subplot(121),imshow(img),title('Imagen');
            subplot(122),imhist(img),title('Histograma');
        end

        function [subImage, rect] = getTargetImage(self, img, plotting)
            % Parámetros opcionales
            switch nargin
                case 2
                    plotting = false;
            end

            img = imadjust(img);
            % Se obtiene el umbral de binarización por medio del
            % histograma, encontrando el valor mínimo de pixeles
            [counts, x] = imhist(img, 128);
            th = round(mean(x(min(counts) == counts)));

            %Se binariza la imagen
            bw = img > th;

            %Se etiqueta la imagen y se obtienen los objetos en ella
            stats = regionprops(bw, 'Centroid', 'Area');

            %% Se busca el objeto con el area mas grande
            if (length(stats) > 0)
                %Si se encuentran mas de un objeto en la imagen,
                % se selecciona como objetivo el objeto con el area mas grande
                object = stats([stats.Area] == max([stats.Area]));

                centroid = round(object.Centroid);
                % Radio del círculo
                r = sqrt(object.Area/pi);
                % Lado del cuadrado contenido dentro del circulo
                l = sqrt(2*(r^2));
                rect = [centroid(1)-l/2, centroid(2)-l/2, l, l];

                % Se recorta la imagen contenida en el rectángulo
                subImage = imadjust(imcrop(img, rect));
            end

            % Mostrar los resultados
            if plotting
                figure(), imshow(bw);
                hold on;
                plot(centroid(1), centroid(2), 'b*');
                rectangle(  'Position', rect,...
                            'EdgeColor','r');
            end;
        end
    end

end

